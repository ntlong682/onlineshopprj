/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.OrderDao;
import dao.OrderDetailDao;
import dao.ProductDao;
import entity.Account;
import entity.AccountDetail;
import entity.Cart;
import entity.Order;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "CheckOutServlet", urlPatterns = {"/check-out"})
public class CheckOutServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
            String name = request.getParameter("name");
            String mobile = request.getParameter("mobile");
            String address = request.getParameter("address");
            String note = request.getParameter("note");
            
            OrderDao od = new OrderDao();
            HttpSession session = request.getSession();
            Account acc = (Account) session.getAttribute("currentLoginAccount");
            if (acc == null) {
                response.sendRedirect("login.jsp");
            } else {
                //add order
                int orderId = od.add(Order.builder()
                        .name(name)
                        .mobile(mobile)
                        .address(address)
                        .note(note)
                        .totalMoney((double) session.getAttribute("totalMoney"))
                        .status(1)
                        .userId(acc.getId())
                        .build());

                //add order details
                if (orderId > 0) {
                    OrderDetailDao odd = new OrderDetailDao();
                    boolean result = odd.add((List<Cart>) session.getAttribute("listCart"), orderId);
                    //System.out.println(result);
                    if (result) {
                        ProductDao dao = new ProductDao();
                        dao.updateQuantityCheckOut((List<Cart>) session.getAttribute("listCart"));
                        session.removeAttribute("listCart");
                        session.removeAttribute("totalProducts");
                        session.removeAttribute("totalMoney");
                        response.sendRedirect("thanks.jsp");
                    } else {
                        //remove order
                        response.sendRedirect("checkout.jsp");
                    }
                    
                } else {
                    //thong bao loi va quay lai check out (meo meo thi day het ve loi =)) )
                    response.sendRedirect("error.jsp");
                }
            }
        } catch (Exception ex) {
            response.sendRedirect("error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

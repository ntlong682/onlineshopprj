/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.BrandDao;
import dao.CategoryDao;
import dao.ProductDao;
import entity.Brand;
import entity.Category;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "UpdateProductServlet", urlPatterns = {"/update-product"})
public class UpdateProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
            System.out.println("VAO DC ROI NE");
//            System.out.println(request.getParameter("id"));
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id);
            String name = request.getParameter("name");
            System.out.println(name);
            int categoryId = Integer.parseInt(request.getParameter("category"));
            int brandId = Integer.parseInt(request.getParameter("brand"));
            double price = Double.parseDouble(request.getParameter("price"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            int status = Integer.parseInt(request.getParameter("status"));
            String imgName = request.getParameter("imgName");
            String description = request.getParameter("description");
            Product product = Product.builder()
                    .id(id)
                    .name(name)
                    .brandId(brandId)
                    .categoryId(categoryId)
                    .price(price)
                    .quantity(quantity)
                    .imgName(imgName)
                    .descripton(description)
                    .status(status)
                    .build();
            List<Brand> lsBrand = new BrandDao().getAll();
            List<Category> lsCategory = new CategoryDao().getAll();
            request.setAttribute("lsBrand", lsBrand);
            request.setAttribute("lsCategory", lsCategory);
            if (new ProductDao().update(id, product)) {
                request.setAttribute("message", "Chỉnh sửa thành công");
                request.setAttribute("product", product);
                request.getRequestDispatcher("edit-product-detail.jsp").forward(request, response);
            } else {
                request.setAttribute("message", "Chỉnh sửa thất bại");
                product = new ProductDao().getOne(id);
                request.setAttribute("product", product);
                request.getRequestDispatcher("edit-product-detail.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            response.sendRedirect("error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

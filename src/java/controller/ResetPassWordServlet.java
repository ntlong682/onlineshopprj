/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AccountDao;
import entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import validation.ValidateString;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "ResetPassWordServlet", urlPatterns = {"/reset-password"})
public class ResetPassWordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String oldPassWord = request.getParameter("oldPassword");
            String newPassWord = request.getParameter("newPassword");
            String conFirmPassword = request.getParameter("conFirmPassword");
            HttpSession session = request.getSession();
            Account acc = (Account) session.getAttribute("currentLoginAccount");
            if (oldPassWord.equals(acc.getPassword())) {
                if (ValidateString.validatePasswordConfirm(newPassWord, conFirmPassword)) {
                    if (new AccountDao().updatePassWordById(acc.getId(), conFirmPassword)) {
                        request.setAttribute("message", "Đổi mật khẩu thành công!");
                        request.getRequestDispatcher("change-password.jsp").forward(request, response);
                    } else {
                        request.setAttribute("message", "Đổi mật khẩu thất bại");
                        request.getRequestDispatcher("change-password.jsp").forward(request, response);
                    }
                } else {
                    request.setAttribute("message", "Mật khẩu không đúng định dạng");
                    request.getRequestDispatcher("change-password.jsp").forward(request, response);
                }
            } else {
                request.setAttribute("message", "Sai mật khẩu");
                request.getRequestDispatcher("change-password.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            response.sendRedirect("error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

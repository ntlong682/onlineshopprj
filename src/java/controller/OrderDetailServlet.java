/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.OrderDetailDao;
import dao.ProductDao;
import entity.Cart;
import entity.OrderDetails;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "OrderDetailServlet", urlPatterns = {"/order-detail"})
public class OrderDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
            int orderId = Integer.parseInt(request.getParameter("id"));
            System.out.println("Order Id " + orderId);
            List<OrderDetails> list = new OrderDetailDao().getOrderDetailsById(orderId);
            //System.out.println(list);
            ArrayList<Cart> listOrderDetail = new ArrayList<>();
//            System.out.println(listOrderDetail);
//            for (int i = 0; i < list.size(); i++) {
//                Product p = new ProductDao().getOne(list.get(i).getId());
//                listOrderDetail.add(Cart.builder()
//                        .id(p.getId())
//                        .name(p.getName())
//                        .price(p.getPrice())
//                        .imgName(p.getImgName())
//                        .quantity(list.get(i).getQuantity())
//                        .build());
//            }
            for (OrderDetails order : list) {
                Product p = new ProductDao().getOne(order.getProductId());
                //System.out.println(p.getId() + " " +p.getName());
                Cart oldOder = (Cart.builder().id(p.getId())
                        .name(order.getProductName())
                        .price(order.getProductPrice())
                        .imgName(p.getImgName())
                        .quantity(order.getQuantity())
                        .build());
                listOrderDetail.add(oldOder);
                //System.out.println("OrderDetail: " + oldOder);
//                listOrderDetail.add(Cart.builder()
//                        .id(p.getId())
//                        .name(order.getProductName())
//                        .price(order.getProductPrice())
//                        .imgName(p.getImgName())
//                        .quantity(order.getQuantity())
//                        .build());
                System.out.println(listOrderDetail);
            }
            request.setAttribute("lsOrderDetail", listOrderDetail);
            request.getRequestDispatcher("order-detail.jsp").forward(request, response);
        } catch (Exception ex) {
            //response.sendRedirect("error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

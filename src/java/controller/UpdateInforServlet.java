/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AccountDetailDao;
import entity.AccountDetail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "UpdateInforServlet", urlPatterns = {"/update-infor"})
public class UpdateInforServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
            HttpSession session = request.getSession();
            AccountDetail accDetail = (AccountDetail) session.getAttribute("currentAccountDetails");
            String name = request.getParameter("name");
            String mobile = request.getParameter("mobile");
            int gender = Integer.parseInt(request.getParameter("gender"));
            String address = request.getParameter("address");
            AccountDetail newAccDetail = AccountDetail.builder()
                    .id(accDetail.getId())
                    .name(name)
                    .phoneNumber(mobile)
                    .gender(gender)
                    .address(address)
                    .build();
            if (new AccountDetailDao().updateAccountDetail(newAccDetail)) {
                session.setAttribute("currentAccountDetails", newAccDetail);
                request.setAttribute("message", "Cập nhật thông tin thành công");
                request.getRequestDispatcher("change-infor.jsp").forward(request, response);
            } else {
                request.setAttribute("message", "Cập nhật thông tin thất bại");
                request.getRequestDispatcher("change-infor.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            response.sendRedirect("error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

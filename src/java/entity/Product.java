/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 *
 * @author Administrator
 */
@Builder
@Setter
@Getter
@ToString
public class Product {
    private int id;
    private int brandId;
    private int categoryId;
    private String name;
    private double price;
    private int quantity;
    private String imgName;
    private String descripton;
    private String note;
    private int status;
}

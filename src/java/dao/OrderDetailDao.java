/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Cart;
import entity.OrderDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import jdbc.SQLServerConnection;

/**
 *
 * @author Administrator
 */
public class OrderDetailDao {
    public boolean add(List<Cart> listCart, int orderId) {
        String query = "INSERT INTO HE140162_order_details VALUES(?,?,?,?,?)";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            for (Cart cart : listCart) {
                
                ps.setInt(1, orderId);
                ps.setInt(2, cart.getId());
                ps.setObject(3, cart.getName());
                ps.setDouble(4, cart.getPrice());
                ps.setInt(5, cart.getQuantity());
                ps.addBatch();
            }
            ps.executeBatch();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return false;
    }
    

    public ArrayList<OrderDetails> getOrderDetailsById(int orderId) {
        String query = "SELECT * FROM HE140162_order_details WHERE order_id=?";
        ArrayList<OrderDetails> listOrder = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, orderId);
//            System.out.println(accId);
            ResultSet rs = ps.executeQuery();
            
        while (rs.next()) { //cau lenh tra ve true,false. No se lay dong dau tien cua ket qua, co thi true va ngc lai
                OrderDetails p = OrderDetails.builder()
                        .id(rs.getInt(1))
                        .productId(rs.getInt(3))
                        .productName(rs.getString(4))
                        .productPrice(rs.getDouble(5))
                        .quantity(rs.getInt(6))
                        .build();
                listOrder.add(p);
            }
            return listOrder;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        }
        return null;
    }
    
    
}

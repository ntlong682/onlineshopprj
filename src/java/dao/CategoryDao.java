/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Category;
import entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.SQLServerConnection;

/**
 *
 * @author Administrator
 */
public class CategoryDao implements IMethod<Category> {

    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    @Override
    public List<Category> getAll() {
        String query = "SELECT * FROM HE140162_category";
        List<Category> ls = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
//            con = SQLServerConnection.getConnection();
//            ps = con.prepareStatement(query);
//            rs = ps.executeQuery();

            while (rs.next()) { //cau lenh tra ve true,false. No se lay dong dau tien cua ket qua, co thi true va ngc lai
                Category p = Category.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .build();
                ls.add(p);
            }
            return ls;
        } catch (SQLException ex) {
            ex.printStackTrace();

        }
        return null;
    }

    @Override
    public Category getOne(int id) {
        String query = "SELECT * FROM HE140162_category\n"
                + "WHERE id = ?"; //vi du them vao query "AND name = ?"
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, id); // 1 la dau cham hoi dau tien
            //ps.setString(2, "abc"); 2 vi dau ? la dau thu 2
            ResultSet rs = ps.executeQuery();
            Category p = null;
            while (rs.next()) {
                p = Category.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .build();
            }
            return p;

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean add(Category obj) {
        String query = "INSERT INTO HE140162_category\nVALUES(?)";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setObject(1, obj.getName());
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return check > 0;
    }

    @Override
    public boolean update(int id, Category obj) {
        String query = "UPDATE HE140162_category SET "
                + "name = ?, "
                + "WHERE id = ?";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setObject(1, obj.getName());
            ps.setObject(2, id);
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Update failed");
        }
        return check > 0;
    }

    @Override
    public boolean remove(int id) {
        String query = "DELETE FROM HE140162_category WHERE id = ?";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setObject(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return check > 0;
    }

}

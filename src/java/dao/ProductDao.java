/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Cart;
import entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbc.SQLServerConnection;

/**
 *
 * @author Administrator
 */
public class ProductDao implements IMethod<Product> {

    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    @Override
    public List<Product> getAll() {
        String query = "SELECT * FROM HE140162_product";
        List<Product> ls = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
//            con = SQLServerConnection.getConnection();
//            ps = con.prepareStatement(query);
//            rs = ps.executeQuery();

            while (rs.next()) { //cau lenh tra ve true,false. No se lay dong dau tien cua ket qua, co thi true va ngc lai
                Product p = Product.builder()
                        .id(rs.getInt("id"))
                        .brandId(rs.getInt("brand_id"))
                        .categoryId(rs.getInt("category_id"))
                        .name(rs.getString("name"))
                        .price(rs.getDouble("price"))
                        .quantity(rs.getInt("quantity"))
                        .imgName(rs.getString("img_name"))
                        .descripton(rs.getString("description"))
                        .note(rs.getString("note"))
                        .status(rs.getInt("status"))
                        .build();
                ls.add(p);
            }
            return ls;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        }
        return null;
    }

    @Override
    public Product getOne(int id) {
        String query = "SELECT * FROM HE140162_product\n"
                + "WHERE id = ?"; //vi du them vao query "AND name = ?"
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, id); // 1 la dau cham hoi dau tien
            //ps.setString(2, "abc"); 2 vi dau ? la dau thu 2
            ResultSet rs = ps.executeQuery();
            Product p = null;
            while (rs.next()) {
                p = Product.builder()
                        .id(rs.getInt("id"))
                        .brandId(rs.getInt("brand_id"))
                        .categoryId(rs.getInt("category_id"))
                        .name(rs.getString("name"))
                        .price(rs.getDouble("price"))
                        .quantity(rs.getInt("quantity"))
                        .imgName(rs.getString("img_name"))
                        .descripton(rs.getString("description"))
                        .note(rs.getString("note"))
                        .status(rs.getInt("status"))
                        .build();
            }
            return p;

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean add(Product obj) {
        String query = "INSERT INTO HE140162_product\nVALUES(?,?,?,?,?,?,?,?,?)";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setObject(1, obj.getBrandId());
            ps.setObject(2, obj.getCategoryId());
            ps.setObject(3, obj.getName());
            ps.setObject(4, obj.getPrice());
            ps.setObject(5, obj.getQuantity());
            ps.setObject(6, obj.getImgName());
            ps.setObject(7, obj.getDescripton());
            ps.setObject(8, obj.getNote());
            ps.setObject(9, obj.getStatus());
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return check > 0;
    }

    @Override
    public boolean update(int id, Product obj) {
        String query = "UPDATE HE140162_product SET " + "brand_id = ?,"
                + "category_id = ?, "
                + "name = ?, "
                + "price = ?, "
                + "quantity = ?, "
                + "img_name = ?, "
                + "description = ?, "
                + "note = ?, "
                + "status = ? "
                + "WHERE id = ?";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setObject(1, obj.getBrandId());
            ps.setObject(2, obj.getCategoryId());
            ps.setObject(3, obj.getName());
            ps.setObject(4, obj.getPrice());
            ps.setObject(5, obj.getQuantity());
            ps.setObject(6, obj.getImgName());
            ps.setObject(7, obj.getDescripton());
            ps.setObject(8, obj.getNote());
            ps.setObject(9, obj.getStatus());
            ps.setObject(10, id);
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Update failed");
        }
        return check > 0;
    }

    @Override
    public boolean remove(int id) {
        String query = "DELETE FROM HE140162_product WHERE id = ?";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setObject(1, id);
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return check > 0;
    }

    public List<Product> searchByName(String text) {
        String query = "SELECT * FROM HE140162_product WHERE name LIKE ?";
        List<Product> ls = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, "%" + text + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product p = Product.builder()
                        .id(rs.getInt("id"))
                        .brandId(rs.getInt("brand_id"))
                        .categoryId(rs.getInt("category_id"))
                        .name(rs.getString("name"))
                        .price(rs.getDouble("price"))
                        .quantity(rs.getInt("quantity"))
                        .imgName(rs.getString("img_name"))
                        .descripton(rs.getString("description"))
                        .note(rs.getString("note"))
                        .status(rs.getInt("status"))
                        .build();
                ls.add(p);
            }
            return ls;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        }
        return null;
    }

    public List<Product> filterByBrand(int id) {
        String query = "SELECT * FROM HE140162_product WHERE brand_id LIKE ?";
        List<Product> ls = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, "%" + id + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product p = Product.builder()
                        .id(rs.getInt("id"))
                        .brandId(rs.getInt("brand_id"))
                        .categoryId(rs.getInt("category_id"))
                        .name(rs.getString("name"))
                        .price(rs.getDouble("price"))
                        .quantity(rs.getInt("quantity"))
                        .imgName(rs.getString("img_name"))
                        .descripton(rs.getString("description"))
                        .note(rs.getString("note"))
                        .status(rs.getInt("status"))
                        .build();
                ls.add(p);
            }
            return ls;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        }
        return null;
    }

    public List<Product> filterByCategory(int id) {
        String query = "SELECT * FROM HE140162_product WHERE category_id LIKE ?";
        List<Product> ls = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, "%" + id + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product p = Product.builder()
                        .id(rs.getInt("id"))
                        .brandId(rs.getInt("brand_id"))
                        .categoryId(rs.getInt("category_id"))
                        .name(rs.getString("name"))
                        .price(rs.getDouble("price"))
                        .quantity(rs.getInt("quantity"))
                        .imgName(rs.getString("img_name"))
                        .descripton(rs.getString("description"))
                        .note(rs.getString("note"))
                        .status(rs.getInt("status"))
                        .build();
                ls.add(p);
            }
            return ls;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        }
        return null;

    }

    public void updateQuantityCheckOut(List<Cart> listCart) {

        ProductDao dao = new ProductDao();
        String query = "UPDATE HE140162_product SET quantity=? WHERE id = ?";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            con.setAutoCommit(false);
            for (Cart cart : listCart) {
                ps.setInt(1, dao.getOne(cart.getId()).getQuantity() - cart.getQuantity());
                ps.setInt(2, cart.getId());
                ps.addBatch();
            }
            ps.executeBatch();
            con.commit();
            ps.clearBatch();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }

//    public static void main(String[] args) {
//        ProductDao a = new ProductDao();
//        a.searchByName("msi");
//        for(int i = 0; i < a.searchByName("msi").size(); i++) {
//            System.out.println(a.searchByName("msi").get(i).toString());
//        }
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jdbc.SQLServerConnection;

/**
 *
 * @author Administrator
 */
public class OrderDao {

    public int add(Order obj) {
        String query = "INSERT INTO HE140162_order\nVALUES(?,?,?,?,?,?,?)";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setObject(1, obj.getName());
            ps.setObject(2, obj.getMobile());
            System.out.println(obj.getMobile());
            ps.setObject(3, obj.getAddress());
            ps.setDouble(4, obj.getTotalMoney());
            ps.setObject(5, obj.getNote());
            ps.setInt(6, obj.getStatus());
            ps.setInt(7, obj.getUserId());
            check = ps.executeUpdate();
            if (check > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.next();
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public ArrayList<Order> getOrderById(int accId) {
        String query = "SELECT * FROM HE140162_order WHERE customerId=?";
        ArrayList<Order> listOrder = new ArrayList<>();
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, accId);
//            System.out.println(accId);
            ResultSet rs = ps.executeQuery();
            
        while (rs.next()) { //cau lenh tra ve true,false. No se lay dong dau tien cua ket qua, co thi true va ngc lai
                Order p = Order.builder()
                        .id(rs.getInt(1))
                        .name(rs.getString(2))
                        .mobile(rs.getString(3))
                        .address(rs.getString(4))
                        .totalMoney(rs.getDouble(5))
                        .note(rs.getString(6))
                        .build();
                listOrder.add(p);
            }
            return listOrder;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        }
        return null;
    }

//    public static void main(String[] args) {
//        Order o = Order.builder()
//                .name("Long")
//                .mobile("0966508900")
//                .address("Ha Noi")
//                .totalMoney(10000)
//                .note("")
//                .status(1)
//                .build();
//        int id = new OrderDao().add(o);
//        System.out.println(id);
//    }
}

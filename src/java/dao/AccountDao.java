/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Account;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import jdbc.SQLServerConnection;

/**
 *
 * @author Administrator
 */
public class AccountDao {

    public Account login(String account, String password) {

        String query = "SELECT * FROM HE140162_account WHERE email = ? AND password = ?";

        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = Account.builder()
                        .id(rs.getInt(1))
                        .email(rs.getString(2))
                        .password(rs.getString(3))
                        .accountDetailId(rs.getInt(4))
                        .roleId(rs.getInt(5))
                        .status(rs.getInt(6))
                        .build();
                return acc;
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public boolean checkDuplicateEmail(String email) {
        String query = "SELECT * FROM HE140162_account WHERE Email = ?";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            return rs.next();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return false;
    }

    public ArrayList<Account> getAllAcount() {
        ArrayList<Account> list = new ArrayList<>();
        String query = "SELECT * FROM HE140162_account";

        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = Account.builder()
                        .id(rs.getInt(1))
                        .email(rs.getString(2))
                        .password(rs.getString(3))
                        .accountDetailId(rs.getInt(4))
                        .roleId(rs.getInt(5))
                        .status(rs.getInt(6))
                        .build();
                list.add(acc);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public boolean addAccount(Account account) {
        int check = 0;
        String query = "INSERT INTO HE140162_account(Email, Password, Account_Detail_Id, Role_ID, Status) VALUES(?,?,?,?,?)";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, account.getEmail());
            ps.setString(2, account.getPassword());
            ps.setInt(3, account.getAccountDetailId());
            ps.setInt(4, account.getRoleId());
            ps.setInt(5, account.getStatus());
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return check > 0;
    }

    public boolean updatePassWordById(int id, String newPassWord) {
        int check = 0;
//        String query = "UPDATE HE140162_account SET password= '"+newPassWord+"' WHERE id =" + id;
        String query = "UPDATE HE140162_account SET password= ? WHERE id =?";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, newPassWord);
            ps.setInt(2, id);
            check = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return check > 0;
    }

    public int updateStatusWhenLoginLogOut(int id, int status) {
        String query = "UPDATE HE140162_account SET status= ? WHERE id =?";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return status;
    }

//    public static void main(String[] args) {
//        AccountDao dao = new AccountDao();
//        System.out.println(dao.updatePassWordById(2, "longnthe140162"));
//    }
}

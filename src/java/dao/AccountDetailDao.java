/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Account;
import entity.AccountDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import jdbc.SQLServerConnection;

/**
 *
 * @author Administrator
 */
public class AccountDetailDao {

    public AccountDetail getOneAccountDetail(int id) {
        String query = "SELECT * FROM HE140162_account_details WHERE id = ?";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountDetail acc = AccountDetail.builder()
                        .id(rs.getInt(1))
                        .name(rs.getString(2))
                        .phoneNumber(rs.getString(3))
                        .gender(rs.getInt(4))
                        .address(rs.getString(5))
                        .build();
                return acc;
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public int addAccountDetail(AccountDetail accountDetail) {
        String query = "INSERT INTO HE140162_account_details(Name, Phone_Number, Gender, Address) VALUES(?,?,?,?)";
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, accountDetail.getName());
            ps.setString(2, accountDetail.getPhoneNumber());
            ps.setInt(3, accountDetail.getGender());
            ps.setString(4, accountDetail.getAddress());
            ResultSet rs = null;
            int check = ps.executeUpdate();
            if (check > 0) {
                rs = ps.getGeneratedKeys();
                rs.next();
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return 0;

    }

    public boolean updateAccountDetail(AccountDetail updatedAccDetail) {
        String query = "UPDATE HE140162_account_details SET Name=?,"
                + " Phone_Number=?,"
                + " Gender=?,"
                + " Address=?"
                + " WHERE id=?";
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection();
                PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, updatedAccDetail.getName());
            ps.setString(2, updatedAccDetail.getPhoneNumber());
            ps.setInt(3, updatedAccDetail.getGender());
            ps.setString(4, updatedAccDetail.getAddress());
            ps.setInt(5, updatedAccDetail.getId());

            check = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return check > 0;
    }
    

}

USE [master]
GO
/****** Object:  Database [SE1430_PRJ321]    Script Date: 3/31/2021 3:30:19 PM ******/
CREATE DATABASE [SE1430_PRJ321]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PuxiGear', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\PuxiGear.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PuxiGear_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\PuxiGear_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [SE1430_PRJ321] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SE1430_PRJ321].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SE1430_PRJ321] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET ARITHABORT OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SE1430_PRJ321] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SE1430_PRJ321] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SE1430_PRJ321] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SE1430_PRJ321] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SE1430_PRJ321] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SE1430_PRJ321] SET  MULTI_USER 
GO
ALTER DATABASE [SE1430_PRJ321] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SE1430_PRJ321] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SE1430_PRJ321] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SE1430_PRJ321] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SE1430_PRJ321] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SE1430_PRJ321] SET QUERY_STORE = OFF
GO
USE [SE1430_PRJ321]
GO
/****** Object:  Table [dbo].[HE140162_account]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[account_detail_id] [int] NULL,
	[role_id] [int] NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_account_details]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_account_details](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Phone_Number] [nvarchar](50) NOT NULL,
	[Gender] [int] NOT NULL,
	[Address] [nvarchar](200) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_brand]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_brand](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_category]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_image]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_image](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NOT NULL,
	[img_name] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_order]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_order](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
	[mobile] [nchar](11) NULL,
	[address] [nvarchar](1000) NULL,
	[total_money] [float] NULL,
	[note] [nvarchar](1000) NULL,
	[status] [int] NULL,
	[customerId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_order_details]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_order_details](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_id] [int] NULL,
	[product_id] [int] NULL,
	[product_name] [nvarchar](100) NULL,
	[product_price] [float] NULL,
	[quantity] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_product]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brand_id] [int] NOT NULL,
	[category_id] [int] NOT NULL,
	[name] [nvarchar](400) NOT NULL,
	[price] [float] NOT NULL,
	[quantity] [int] NOT NULL,
	[img_name] [nchar](100) NOT NULL,
	[description] [nvarchar](2000) NULL,
	[note] [nvarchar](200) NULL,
	[status] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HE140162_role]    Script Date: 3/31/2021 3:30:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HE140162_role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[HE140162_account] ON 

INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (1, N'admin@gmail.com', N'admin123', 1, 1, 1)
INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (2, N'ntlong682@gmail.com', N'longnthe140162', 2, 2, 2)
INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (3, N'test2@gmail.com', N'test123', 5, 2, 1)
INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (4, N'test4@gmail.com', N'test123456', 6, 2, 2)
INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (5, N'testuser@gmail.com', N'testuser123', 7, 2, 2)
INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (6, N'longdz@gmail.com', N'a123456', 8, 2, 2)
INSERT [dbo].[HE140162_account] ([id], [email], [password], [account_detail_id], [role_id], [status]) VALUES (7, N'user@gmail.com', N'user123', 9, 2, 1)
SET IDENTITY_INSERT [dbo].[HE140162_account] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_account_details] ON 

INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (1, N'Long', N'0966508900', 1, N'Ha Noi')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (2, N'Nguyễn Thành Long', N'0966508900', 1, N'FPT, Hoa Lac, Ha Noi')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (3, N'Meo', N'0123456789', 0, N'meo')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (4, N'TEst', N'0987654312', 0, N'Test')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (5, N'test two', N'0987651243', 0, N'test 2')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (6, N'Test Four', N'0987654123', 1, N'Test 4')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (7, N'Test User', N'0987654321', 0, N'Test Address')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (8, N'long dz', N'ahihi', 0, N'0')
INSERT [dbo].[HE140162_account_details] ([id], [Name], [Phone_Number], [Gender], [Address]) VALUES (9, N'Người Dùng', N'0987654321', 1, N'FPT')
SET IDENTITY_INSERT [dbo].[HE140162_account_details] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_brand] ON 

INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (1, N'MSI', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (2, N'DELL', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (3, N'ASUS', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (4, N'ACER', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (5, N'CustomPc', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (6, N'Kingston', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (7, N'Corsair', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (8, N'Logitech', 1)
INSERT [dbo].[HE140162_brand] ([id], [name], [status]) VALUES (9, N'Intel', 1)
SET IDENTITY_INSERT [dbo].[HE140162_brand] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_category] ON 

INSERT [dbo].[HE140162_category] ([id], [name]) VALUES (1, N'Laptop')
INSERT [dbo].[HE140162_category] ([id], [name]) VALUES (2, N'PC')
INSERT [dbo].[HE140162_category] ([id], [name]) VALUES (3, N'CPU')
INSERT [dbo].[HE140162_category] ([id], [name]) VALUES (4, N'RAM')
INSERT [dbo].[HE140162_category] ([id], [name]) VALUES (5, N'Monitor')
INSERT [dbo].[HE140162_category] ([id], [name]) VALUES (6, N'Mouse')
SET IDENTITY_INSERT [dbo].[HE140162_category] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_image] ON 

INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (7, 1, N'id1img1.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (8, 1, N'id1img2.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (9, 1, N'id1img3.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (10, 1, N'id1img4.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (11, 2, N'id2img1.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (12, 2, N'id2img2.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (13, 2, N'id2img3.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (14, 2, N'id2img4.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (15, 3, N'id3img1.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (16, 3, N'id3img2.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (17, 3, N'id3img3.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (18, 3, N'id3img4.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (19, 4, N'id4img1.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (20, 4, N'id4img2.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (21, 4, N'id4img3.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (22, 4, N'id4img4.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (23, 8, N'id8img1.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (24, 8, N'id8img2.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (25, 8, N'id8img3.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (27, 8, N'id8img4.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (28, 11, N'id11img1.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (29, 11, N'id11img2.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (30, 11, N'id11img3.jpg')
INSERT [dbo].[HE140162_image] ([id], [product_id], [img_name]) VALUES (31, 11, N'id11img4.jpg')
SET IDENTITY_INSERT [dbo].[HE140162_image] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_order] ON 

INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (18, N'Nguyễn Thành Long', N'0966508900 ', N'FPT', 98800000, N'Khum có gì', 1, 2)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (20, N'Sơn', N'098715263  ', N'Hà Nội', 120289000, N'Ship COD', 1, 2)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (21, N'Nguyen Thanh Long', N'0966508900 ', N'FPT, Hoa Lac, Ha Noi', 251378000, N'test batch update', 1, 2)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (22, N'Nguyen Thanh Long', N'0966508900 ', N'FPT, Hoa Lac, Ha Noi', 251378000, N'test batch', 1, 2)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (23, N'Nguyen Thanh Long', N'0966508900 ', N'FPT, Hoa Lac, Ha Noi', 219989000, N'test b', 1, 2)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (24, N'Test User', N'0987654321 ', N'Test Address', 9196000, N'Test User buy product', 1, 5)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (25, N'Test User', N'0987654321 ', N'Test Address', 4598000, N'test user product 2', 1, 5)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (28, N'Nguyễn Thành Meo Meo', N'0966856230 ', N'place Long live', 112578000, N'Hoàng tặng, love from nademan <3 <3', 1, 2)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (29, N'long dz', N'ahihi      ', N'0', 88999000, N'égg', 1, 6)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (30, N'long dz', N'ahihi      ', N'0', 85194000, N'test2', 1, 6)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (31, N'long dz', N'ahihi      ', N'0', 0, N'XD
', 1, 6)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (32, N'long dz', N'ahihi      ', N'0', 467183000, N'', 1, 6)
INSERT [dbo].[HE140162_order] ([id], [name], [mobile], [address], [total_money], [note], [status], [customerId]) VALUES (19, N'Phạm Hoàng', N'0987635142 ', N'FPT', 154000000, N'Ship trong giờ hành chính', 1, 2)
SET IDENTITY_INSERT [dbo].[HE140162_order] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_order_details] ON 

INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (15, 18, 2, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (16, 18, 3, N'Laptop Dell Vostro 3401 (70227394)', 10800000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (19, 20, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56289000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (20, 20, 2, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (21, 21, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56289000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (22, 21, 2, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (23, 21, 3, N'Laptop Dell Vostro 3401 (70227394)', 10800000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (24, 22, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56289000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (25, 22, 2, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (27, 23, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56289000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (28, 23, 8, N'PC ENTHUSIAST GAMING PLATINUM 03', 77000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (29, 23, 11, N'PC Asus ROG Strix G15DH ', 28900000, 3)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (30, 24, 19, N'Ram Corsair LPX 16GB DDR4 2666MHz', 2299000, 4)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (31, 25, 19, N'Ram Corsair LPX 16GB DDR4 2666MHz', 2299000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (32, 28, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56289000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (33, 29, 2, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (34, 29, 3, N'Laptop Dell Vostro 3401 (70227394)', 10800000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (35, 29, 22, N'Màn hình Asus ROG XG27WQ', 14199000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (36, 30, 22, N'Màn hình Asus ROG XG27WQ', 14199000, 6)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (37, 32, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56289000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (38, 32, 2, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (17, 18, 4, N'Laptop Asus ZenBook UX434FLC-A6212T', 24000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (18, 19, 8, N'PC ENTHUSIAST GAMING PLATINUM 03', 77000000, 2)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (26, 22, 3, N'Laptop Dell Vostro 3401 (70227394)', 10800000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (39, 32, 3, N'Laptop Dell Vostro 3401 (70227394)', 10800000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (40, 32, 4, N'Laptop Asus ZenBook UX434FLC', 24000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (41, 32, 8, N'PC ENTHUSIAST GAMING PLATINUM 03', 77000000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (42, 32, 13, N'PC ENTHUSIAST GAMING LIMITED 004', 147999000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (43, 32, 14, N'PC GAMING SHARK 042', 16999000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (44, 32, 11, N'PC Asus ROG Strix G15DH ', 28900000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (45, 32, 15, N'PC GAMING LION 0123-MK', 7999000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (46, 32, 23, N'CPU Intel Core i9-9900KF', 12599000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (47, 32, 24, N'CPU Intel Core i9-9940X', 19799000, 1)
INSERT [dbo].[HE140162_order_details] ([id], [order_id], [product_id], [product_name], [product_price], [quantity]) VALUES (48, 32, 21, N'Chuột Logitech G402 Hyperion USB', 799000, 1)
SET IDENTITY_INSERT [dbo].[HE140162_order_details] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_product] ON 

INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (1, 1, 1, N'Laptop MSI Gaming GE66 Raider 10SFS', 56000000, 4, N'id1img1.jpg                                                                                         ', N'Hãng sản xuất: MSI </br>
Chủng loại: GE66 Raider 10SF </br>
Part Number: 474VN </br>
Mầu sắc: Xám bạc </br>
Bộ vi xử lý: Intel Core i7-10875H (2.3Ghz upto 5.1Ghz/16MB cache) </br>
Chipset: Intel® HM470 </br>
Bộ nhớ trong: 32GB (2x 16GB DDR4-3200) </br>
Số khe cắm: 2 </br>
Dung lượng tối đa: 64GB </br>
VGA: NVIDIA® GeForce RTX™ 2070 Super with 8GB GDDR6 </br>
Ổ cứng: 1TB NVMe PCIe Gen3x4 SSD </br>
Khả năng lưu trữ: </br>
 + 1x M.2 SSD slot (NVMe PCIe Gen3) </br>
 + 1x M.2 SSD Combo slot (NVMe PCIe Gen3 / SATA)</br>
 + 1x 2.5" SATA HDD</br>
Ổ quang: Không </br>
Card Reader: N/A</br>
Keyboard: Per-Key RGB Backlight Keyboard</br>
Màn hình: 15.6 inch FHD (1920*1080), 300Hz Thin Bezel</br>
Webcam: HD type (30fps@720p)</br>
Audio:</br>
 + 2x 3W Speaker</br>
 + 2x 3W Woofer</br>
Giao tiếp mạng: Killer Gb LAN</br>
Giao tiếp không dây: Killer axWi-Fi + Bluetooth v5.1</br>
Cổng giao tiếp:</br>
 + 1x RJ45</br>
 + 1x SD (XC/HC) Card Reader</br>
 + 1x (4K @ 60Hz) HDMI</br>
 + 1x Mini-DisplayPort</br>
 + 2x Type-A USB3.2 Gen1</br>
 + 1x Type-A USB3.2 Gen2</br>
 + 1x Type-C (USB3.2 Gen2 / DP)</br>
 + 1x Type-C USB3.2 Gen2x2</br>
Pin: 4-Cell, 51 Battery (Whr)</br>
Kích thước (rộng x dài x cao): N/A</br>
Cân nặng: 2.38 kg</br>
Hệ điều hành: Windows 10 Home</br>
Phụ kiện đi kèm: 280W adapter</br>', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (2, 1, 1, N'Laptop MSI Gaming GE75 Raider 10SFS', 64000000, 11, N'id2img1.jpg                                                                                         ', N'Hãng sản xuất

MSI

Chủng loại

GE75 Raider 10SFS

Part Number

270VN

Mầu sắc

Đen

Bộ vi xử lý

Intel Core I9 10980HK – thế hệ 10 mới nhất

Chipset

Intel® HM470

Bộ nhớ trong

16GB (2x 8GB DDR4-3200Mhz)

Số khe cắm

2

Dung lượng tối đa

64GB

VGA

NVIDIA® GeForce RTX™ 2070 super with 8GB GDDR6

Ổ cứng

512GB NVMe PCIe SSD +1TB (SATA) 7200rpm

Ổ quang

N/A

Card Reader

1x SD (XC/HC)

Keyboard

Per-Key RGB Backlight Keyboard

Màn hình

17.3 inch FHD (1920x1080), 240Hz, IPS-Level

Webcam

HD type (30fps@720p)

Audio

2x 3W Speaker, 2x 3W Woofer', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (3, 2, 1, N'Laptop Dell Vostro 3401 (70227394)', 10800000, 22, N'id3img1.jpg                                                                                         ', N'Hãng sản xuất

Dell

Chủng loại

Vostro 3401 – Bảo hành tận nơi

Part number

70227394

Mầu sắc

Đen

Bộ vi xử lý

Intel Core i3-1005G1 (1.2Ghz/4MB cache)

Chipset

Intel

Bộ nhớ trong

4GB DDR4 2667Mhz

Số khe cắm

2

Dung lượng tối đa

 

VGA

Intel UHD

Ổ cứng

1TB HDD (có khe cắm M2 SSD)

Ổ quang

No

Card Reader

Micro SD

Bảo mật, công nghệ

 

Màn hình

14-inch, HD, 1366x768

Webcam

HD

Audio

2W Dual stereo speakers

Giao tiếp mạng

Gigabit

Giao tiếp không dây

802.11ac ,Bluetooth 5.0

Cổng giao tiếp

1 USB 2.0 port, 2 USB 3.2 Gen 1 port, 1 HDMI 1.4, 1 Universal Audio Jack

Pin

3 cell 42Wh

Kích thước

 

Cân nặng

1.64Kg

Hệ điều hành

Win 10 Home SL 64

Phụ kiện đi kèm

AC Adapter', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (4, 3, 1, N'Laptop Asus ZenBook UX434FLC', 24000000, 12, N'id4img1.jpg                                                                                         ', N'Hãng sản xuất

Asus

Chủng loại

Zenbook

Part Number

UX434FLC-A6212T

Mầu sắc

Bạc

Bộ vi xử lý

Intel® Core™ i5-10210U Processor 1.6 GHz (6M Cache, up to 4.2 GHz)

Chipset

N/A

Bộ nhớ trong

8GB on board

Số khe cắm

N/A

Dung lượng tối đa

8G

VGA

NVIDIA GeForce MX250 - GDDR5 2GB

Ổ cứng

PCIEG3x2 NVME 512GB M.2 SSD

Ổ quang

N/A

Card Reader

Type: Micro SD

Bảo mật, Công nghệ

TPM (Firmware TPM)

McAfee Internet Security(Trial version)

Illuminated Chiclet Keyboard

ScreenPad™

Màn hình

14.0''//Narrow border//300nits//FHD 1920x1080 16:9//Glare//NTSC: 72%//Wide View

Webcam

HD IR Camera

Audio

Built-in speaker

Built-in array microphone

Audio by ICEpower®

harmon/ kardon

Giao tiếp mạng

N/A

Giao tiếp không dây

802.11ax+Bluetooth 5.0 (Dual band) 2*2

Cổng giao tiếp

1x USB 2.0

1x USB3.1 Type A (Gen2)

1x USB3.1 Type C (Gen 2)

1x Headphone-out & Audio-in Combo Jack

1x HDMI

Pin

50WHrs, 3S1P, 3-cell Li-ion

Kích thước

31.9(W) x 19.9(D) x 1.69 ~ 1.82 (H) cm

Cân nặng

1.10 KG (Without Battery)

 1.26 KG (with 3 cell battery)

Hệ điều hành

Windows 10 (64bit)

Phụ kiện đi kèm

Sleeve, USB3.0 to RJ45 Cable

', N'', 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (8, 5, 2, N'PC ENTHUSIAST GAMING PLATINUM 03', 77000000, 7, N'id8img1.jpg                                                                                         ', N'Mainboard

ASUS ROG STRIX Z490-E GAMING

1

CPU

Intel Core i9-10900K (3.7GHz turbo up to 5.3GHz, 10 nhân 20 luồng, 20MB Cache, 125W)

1

RAM

Ram Gskill Trident Z Neo 16GB (2x8GB) DDR4 3600MHz

2

SSD

SSD Samsung 970 EVO Plus 500GB PCIe NVMe

1

VGA

Card Asus ROG STRIX RTX 2080 SUPER O8G GAMING

1

NGUỒN

Corsair RM Series RM850 - 850 Watt 80 Plus Gold Certified Fully Modular

1

VỎ CASE

Case EK-Classic InWin 303EK - Black

1

FAN LED

Fan Case CoolerMaster MasterFan MF120 Halo 3in1

2

 

Bộ đèn LED dán case Ambino Node Rainbow ( Hub + 2 x LED D-RGB 25cm )

2

 

EK-Velocity RGB - Nickel + Plexi

1

 

EK-Vector Strix RTX 2080 RGB - Nickel + Plexi

1

 

EK-Vector Strix RTX 2080 Backplate - Nickel

1

 

EK-CryoFuel Clear (Premix 1000mL)

1

 

Dây Nguồn bọc lưới LIAN LI Strimer RGB 24 PIN

1

 

Dây Nguồn bọc lưới LIAN LI Strimer RGB 2x8 PIN

1

 

EK-RGB 4-Way Splitter Cable

2

PHỤ KIỆN

EK-Torque HTC-12 Color Rings Pack - Red

2

 

EK-Torque HTC-12 - Nickel

14

 

Fitting EK-AF Angled 2×45° G1/4 Nickel

5

 

Fitting EK-AF Classic Angled 90° - Nickel

6

 

Fitting EK-CSQ Plug G1/4 (for EK-Badge) - Nickel

3

 

EK-AF Extender 30mm M-F G1/4 - Nickel

1

 

Radiator EK-CoolStream SE 360 (Slim Triple)

1

 

Radiator EK-CoolStream Classic PE 360

1

 

Fitting EK-AF Extender 20mm M-F G1/4 - Nickel

3

 

EK-HD Tube 10/12mm 500mm (2 pcs)

4

HỆ ĐIỀU HÀNH

Dos', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (13, 5, 2, N'PC ENTHUSIAST GAMING LIMITED 004', 147999000, 4, N'54394_pcgm229.jpg                                                                                   ', N'Hệ thống PC ENTHUSIAST GAMING LIMITED 004 là một trong những bộ máy cao cấp nhất mà HNC đã xây dựng. Mang trong mình cấu hình phần cứng với CPU Ryzen 9 3960X, card đồ hoạ RTX 2080Ti và 64GB RAM, chiếc máy này thừa khả năng xử lý mọi thứ bạn yêu cầu ở đẳng cấp cao nhất.

CHIẾC PC NÀY DÀNH CHO AI?
Những người có khả năng tài chính cao yêu thích sự cá tính, bóng bẩy, đồng thời cũng yêu cầu một cấu hình tương xứng. Những người muốn HNC custom dàn máy hoàn toàn theo ý thích của họ và có quyền góp ý chỉnh sửa trong suốt quá trình thi công.', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (14, 5, 2, N'PC GAMING SHARK 042', 16999000, 19, N'56638_pcgm317_2021.jpg                                                                              ', N'N/A', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (11, 5, 2, N'PC Asus ROG Strix G15DH ', 28900000, 2, N'id11img1.jpg                                                                                        ', N'Hãng sản xuất

Asus

Chủng loại

ROG Strix G15DH

Part Number

G15DH-VN001T

Kiểu dáng

Tower

Bộ vi xử lý

AMD Ryzen 7 3700X

Chipset

B450

Bộ nhớ trong

16GB DDR4 3200Mhz

Số khe cắm

2

VGA

NVIDIA GeForce RTX2060S

Ổ cứng

512GB SSD

Giao tiếp mạng

10/100/1000/Gigabits Mbps

802.11 ac

Giao tiếp không dây

Bluetooth 5.1

Audio

Realtek® ALC887

Cổng giao tiếp

Mặt trước:

1 x USB 3.2 Gen 1 type A

1 x USB 3.2 Gen 1 type C

1 x Mic in

1 x Headphone out

 

Mặt sau:

2 x PS/2

1 x DVI-D

1 x VGA

1 x RJ45

2 x USB 3.2 Gen 2

4 x USB 3.2 Gen 1

3 x Audio jacks

Bàn phím & Chuột

Không có

Kích thước (WxDxH)

18.9 x 42.1 x 49.8 (cm)

Trọng lượng

10kg

Hệ điều hành

Windows 10', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (15, 5, 2, N'PC GAMING LION 0123-MK', 7999000, 9, N'53169_pcgm309.jpg                                                                                   ', N'Hệ thống PC Gaming Lion 004 với những thiết bị phần cứng như CPU Ryzen 3 3200G, RAM 4GB. Đóng vai trò là những bộ máy gaming cơ bản nhất dành cho người mới bắt đầu của HANOICOMPUTER

CHIẾC PC NÀY DÀNH CHO AI?
Những người mới bước chân vào lĩnh vực giải trí trên máy tính, muốn tìm hiểu về sự tiện lợi của máy bàn trước khi đưa ra quyết định đầu tư dài hơi vào nền tảng làm việc, giải trí này.', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (16, 6, 4, N'DDRam 4 Kingston ECC 32GB/2666', 6190000, 20, N'47682_kingston_ecc__32gb2666_ksm26rd432hai.jpg                                                      ', N'ECC:  viết tắt của cụm từ Error Checking and Correction – nghĩa là kiểm tra và sửa lỗi.

RAM ECC: là thanh RAM ECC có khả năng tự động sửa lỗi,có khả năng điều khiển được dòng dữ liệu ra và vào.

Với Ram ECC: khi có lỗi xảy ra nó chỉ cần yêu cầu gửi lại đúng gói tin bị lỗi và sau đó tiếp tục tiến trình làm việc( vì RAM ECC quản lý được dòng dữ liệu, tự sửa lỗi ) , đem đến sự ổn định, tránh rủi ro cho người dùng.

RAM ECC REG là gì (Registered Memory) ?

Là loại SDRAM có các thanh ghi (register) được gắn trực tiếp trên module nhớ. Các thanh ghi (register) tái định hướng (re-drive) các tín hiệu qua các chip nhớ và cho phép module chứa nhiều chip nhớ hơn. Registered memory và unbuffered memory không thể được dùng chung với nhau trong một máy tính.', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (19, 7, 4, N'Ram Corsair LPX 16GB DDR4 2666MHz', 2299000, 14, N'56986_ram_desktop_corsair_vengeance_lpx_cmk16gx4m2d2666c16_16gb_2x8gb_ddr4_2666mhz.jpg              ', N'Ram DDR4 Corsair Vengeance LPX được thiết kế để ép xung hiệu năng cao. Bộ tản nhiệt được làm bằng nhôm nguyên chất giúp tản nhiệt nhanh hơn, và PCB tám lớp giúp quản lý nhiệt và cung cấp khoảng không ép xung vượt trội. Mỗi IC được sàng lọc riêng cho hiệu suất cao nhất.', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (20, 3, 6, N'Chuột Asus ROG Gladius II Core', 1299000, 8, N'53593_asus_rog_gladius_ii_core__p507_.jpg                                                           ', N'Chuột Chơi game Asus ROG Gladius II Core
Thiết kế công thái học cổ điển quen thuộc của dòng chuột Gladius
Thiết kế gọn nhẹ, các nút trái, phải riêng biệt, 2 bên hông được khắc rãnh chống trượt mang phong cách ROG
Mắt cảm biến PAW3327, độ phân giải 6200 DPI tracking nhanh và chính xác
Thiết kế thay nóng switch bấm độc quyền ROG, tăng độ bền và tuổi thọ của chuột
Phần mềm ASUS Armory II trực quan để dễ dàng tùy chỉnh các nút, hiệu suất và cài đặt Led', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (21, 8, 6, N'Chuột Logitech G402 Hyperion USB', 799000, 14, N'29034_tong_the_mouse_logitech_g402_usb_black.jpg                                                    ', N'Chuột Game FPS nhanh nhất thế giới !
Chuột Logitech G402 sử dụng cảm biến quang học công nghệ Logitech Delta Zero™ tích hợp Fusion Engine™ cho tốc độ lên đến 500IPS.
Tích hợp 8 nút có thể lập trình được, gán các tác vụ khác nhau được trên chuột.
Cho phép chuyển đổi DPI từ 250 DPI đến 4000 DPI ngay tức thì bằng phím bấm trên chuột.
Sử dụng bộ xử lý 32 BIT ARM để đảm bảo mọi tác vụ được xử lý nhanh chóng , tốc độ, chính xác,... một cách thông minh.', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (22, 3, 5, N'Màn hình Asus ROG XG27WQ', 14199000, 0, N'54076_xg27wq_05__6_.jpg                                                                             ', N'27-inch WQHD (2560x1440) màn hình cong với tốc độ làm tươi 165Hz được thiết kế cho các game thủ chuyên nghiệp và gameplay nhập vai
Công nghệ ASUS Extreme Low Motion blur (ELMB™) cho phép thời gian phản hồi 1ms (MPRT) cùng với đồng bộ hóa Adaptive, loại bỏ bóng ma và rách hình
FreeSync™ Premium Pro
Dynamic Range (HDR) công nghệ với gam màu chuyên nghiệp', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (23, 9, 3, N'CPU Intel Core i9-9900KF', 12599000, 6, N'45160_hnc_intel_i9_9900kf_right_facing_850x850.jpg                                                  ', N'Phiên bản cắt giảm đi nhân đồ họa tích hợp của 9900K
Số nhân: 8
Số luồng: 16
Tốc độ cơ bản: 3.6 GHz
Tốc độ tối đa: 5.0 GHz
Cache: 16MB', NULL, 1)
INSERT [dbo].[HE140162_product] ([id], [brand_id], [category_id], [name], [price], [quantity], [img_name], [description], [note], [status]) VALUES (24, 9, 3, N'CPU Intel Core i9-9940X', 19799000, 4, N'45081_hnc_intel_i9_10900x_right_facing_850x850.jpg                                                  ', N'Tiến trình sản xuất 14 nm
14 nhân, 28 luồng, xung nhịp cơ bản 3.3 GHz, xung nhịp Boost tối đa 4.4 GHz
Hỗ trợ RAM DDR4
Không đi kèm quạt tản nhiệt
Đã mở khóa hệ số nhân
Phù hợp với các bo mạch chủ Socket LGA 2066', NULL, 1)
SET IDENTITY_INSERT [dbo].[HE140162_product] OFF
GO
SET IDENTITY_INSERT [dbo].[HE140162_role] ON 

INSERT [dbo].[HE140162_role] ([id], [Name]) VALUES (1, N'admin')
INSERT [dbo].[HE140162_role] ([id], [Name]) VALUES (2, N'customer')
SET IDENTITY_INSERT [dbo].[HE140162_role] OFF
GO
USE [master]
GO
ALTER DATABASE [SE1430_PRJ321] SET  READ_WRITE 
GO

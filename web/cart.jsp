<%-- 
    Document   : cart
    Created on : Mar 2, 2021, 8:21:41 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <style>
            a {
                text-decoration: none;
            }
        </style>
        <title>Cart</title>
    </head>
    <body>
        <%@include file="component/navbar.jsp" %>
        <c:choose>
            <c:when test="${sessionScope.listCart eq null}">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h4> Không có sản phẩm nào trong giỏ hàng</h4>
                            <a class="btn btn-info" href="ProductServlet">Tiếp tục mua hàng</a>
                        </div>
                    </div>
                </div>

            </c:when>
            <c:otherwise>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Hình ảnh</th>
                                        <th>Tên</th>
                                        <th>Giá</th>
                                        <th>Số lượng</th>
                                        <th>Số tiền</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${sessionScope.listCart}" var="cart">
                                        <tr>
                                            <td>
                                                <img src="assets/images/products/${cart.imgName}" style="width: 80px">
                                            </td>
                                            <td>${cart.name}</td>
                                            <td><fmt:formatNumber type="currency" currencySymbol="đ" value="${cart.price}" /></td>

                                            <td>
                                                <a class="me-3" href="change-quantity-cart?type=0&id=${cart.id}">-</a>
                                                ${cart.quantity}
                                                <a class="ms-3" href="change-quantity-cart?type=1&id=${cart.id}">+</a>
                                            </td>
                                            <td>
                                                <fmt:formatNumber type="currency" currencySymbol="đ" value="${cart.price * cart.quantity}" />
                                            </td>

                                            <td>
                                                <a href="remove-cart?id=${cart.id}">Xóa</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-info" href="ProductServlet">Tiếp tục mua hàng</a>
                        </div>
                        <div class="col-md-6 text-end">
                            <b class="me-4">Tổng tiền:
                                <fmt:formatNumber type="currency" currencySymbol="đ" value="${sessionScope.totalMoney}" />
                                <!--format o day-->
                            </b>
                            <a class="btn btn-danger me-2" href="remove-cart?id=0">Xóa tất cả</a>
                            <a class="btn btn-success" href="checkout.jsp">Tiếp tục</a>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <footer>
            <%@include file="component/footer.jsp" %>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>
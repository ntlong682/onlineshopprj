<%-- 
    Document   : order-details
    Created on : Mar 15, 2021, 12:49:07 AM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/user.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>User Detail</title>
    </head>
    <body>
        <%@include file="component/user-navbar.jsp" %>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3">
                    <div class="left-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>Danh mục quản lý</h6>
                            </div>
                        </div>
                        <%@include file="component/user-sidenav.jsp"%>   
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Danh mục đơn hàng
                                </h6>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Họ và tên</th>
                                                <th scope="col">Số diện thoại</th>
                                                <th scope="col">Địa chỉ</th>
                                                <th scope="col">Tổng tiền</th>
                                                <th scope="col">Ghi chú</th>
                                                <th scope="col">Chi tiết</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach  items="${requestScope.lsOrderOfCustomer}" var="order">
                                                <tr>
                                                    <td> ${order.name}</td>
                                                    <td> ${order.mobile}</td>
                                                    <td> ${order.address}</td>
                                                    <td> <fmt:formatNumber type="currency" currencySymbol="đ" value="${order.totalMoney}" /></td>
                                                    <td> ${order.note}</td>
                                                    <td>
                                                        <a href="order-detail?id=${order.id}" class="g-color">Chi tiết</a>
                                                    </td>
                                                </tr>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>

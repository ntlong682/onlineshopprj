<%-- 
    Document   : manage-account
    Created on : Mar 17, 2021, 4:46:50 AM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/user.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>User Detail</title>
    </head>
    <body>
        <%@include file="component/admin-nav.jsp" %>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3">
                    <div class="left-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>Danh mục quản lý</h6>
                            </div>
                        </div>
                        <%@include file="component/admin-sidenav.jsp"%>   
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-side"> 
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Danh sách tài khoản
                                </h6>
                            </div>
                        </div> 
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">STT</th>
                                                <th scope="col">Mã tài khoản</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Loại tài khoản</th>
                                                <th scope="col">Tình trạng</th>
                                                <th scope="col">Thao tác</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${requestScope.lsAccount}" var="acc" varStatus="stt">
                                                <tr>
                                                    <td>${stt.index+1}</td>
                                                    <td>${acc.id}</td>
                                                    <td>${acc.email}</td>
                                                    <td>${acc.roleId == 1 ? "Admin" : "Khách hàng"}</td>
                                                    <td>${acc.status == 1 ? "Đang hoạt động" : acc.status == 2 ? "Dừng hoạt động" : "Tài khoản đã vô hiệu hóa"}</td>
                                                    <td>
                                                        <a href="edit-account?id=${acc.id}">Cập nhật</a>
                                                    </td>
                                                </tr>
                                            </c:forEach>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>
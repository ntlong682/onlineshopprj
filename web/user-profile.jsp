<%-- 
    Document   : user-profile
    Created on : Mar 14, 2021, 9:01:35 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/user.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>User Detail</title>
    </head>
    <body>
        <%@include file="component/user-navbar.jsp" %>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3">
                    <div class="left-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>Danh mục quản lý</h6>
                            </div>
                        </div>
                        <%@include file="component/user-sidenav.jsp"%>   
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Thông tin tài khoản
                                </h6>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <table>
                                        <tr>
                                            <td>Email:</td>
                                            <td><b>${currentLoginAccount.email}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Mật khẩu:</td>
                                            <td>
                                                <a href="change-password.jsp">Đổi mật khẩu</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Loại tài khoản:</td>
                                            <td>${currentLoginAccount.roleId == 1 ? "Admin" : "Khách hàng"}</td>
                                        </tr>
                                        <tr>
                                            <td>Trạng thái:</td>
                                            <td>
                                                <c:if test="${currentLoginAccount.status == 1}">
                                                    <span class="active">Khả dụng</span>
                                                </c:if>
                                                <c:if test="${currentLoginAccount.status != 1}">
                                                    <span class="deactive">Không khả dụng</span>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Thông tin cá nhân
                                </h6>
                            </div>
                        </div> 
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <table>
                                        <tr>
                                            <td>Họ và tên:</td>
                                            <td><b>${currentAccountDetails.name}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Số điện thoại:</td>
                                            <td>${currentAccountDetails.phoneNumber}</td>
                                        </tr>
                                        <tr>
                                            <td>Giới tính:</td>
                                            <td>${currentAccountDetails.gender == 1 ? "Nam" : "Nữ"}</td>
                                        </tr>
                                        <tr>
                                            <td>Địa chỉ:</td>
                                            <td>${currentAccountDetails.address}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="change-infor.jsp">Thay đổi thông tin</a>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>
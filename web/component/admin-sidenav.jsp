<%-- 
    Document   : user-sidenav
    Created on : Aug 24, 2018, 7:57:01 PM
    Author     : Shado
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- User Side Nav -->
<div class="row">
    <div class="col-md-12">
        <div class="side-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="admin-index.jsp">
                        <i class="mr-2"></i> Thông tin tài khoản
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="manage-product">
                        <i class="mr-2"></i> Thông tin sản phẩm
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="manage-account">
                        <i class="mr-2"></i> Thông tin khách hàng
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="mr-2"></i> Thông tin đơn hàng
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div> 
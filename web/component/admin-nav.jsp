<%-- 
    Document   : admin-nav
    Created on : Aug 24, 2018, 7:57:01 PM
    Author     : Shado
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--<style>
    a:hover{
        color: blue !important;
    }
    a {
        display: block;
        text-decoration: none;
        color: black;
        text-transform: uppercase;
        font-weight: 500;
    }
</style>-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/Longnthe140162Shop/admin-index.jsp">LAZI GEAR</a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <c:if test="${currentLoginAccount ne null}">   
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            ${currentLoginAccount.email}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="admin-index.jsp">Thông tin cá nhân</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="logout">Đăng xuất</a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${currentLoginAccount eq null}">
                    <li class="nav-item">
                        <a class="nav-link" href="register.jsp">Đăng ký</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="login.jsp">Đăng nhập</a>
                    </li>
                </c:if>
            </ul>

        </div>
    </div>
</nav>
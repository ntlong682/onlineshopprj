<%-- 
    Document   : index
    Created on : Jan 11, 2021, 8:40:31 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <style>
            .link-edit:hover{
                color: blue !important;
            }
            .link-edit {
                display: block;
                text-decoration: none;
                color: black;
                text-transform: uppercase;
                font-weight: 500;
            }
        </style>
        <title>Home</title>
    </head>
    <body>
        <%@include file="component/navbar.jsp" %>
        <div class="container mt-5"> 
            <div class="row">
                <div class="col-md-2">
                    <h5>Thương hiệu</h5>
                    <c:forEach items="${requestScope.lsBrand}" var="brand">
                        <a class="link-edit" href="filter?id=${brand.id}&&type=1">${brand.brandName}</a>
                    </c:forEach>
                    <h5 class="mt-2">Thể loại</h5>
                    <c:forEach items="${requestScope.lsCategory}" var="category">
                        <a class="link-edit" href="filter?id=${category.id}&&type=2">${category.name}</a>
                    </c:forEach>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <c:forEach items="${requestScope.lsProduct}" var="product">
                            <div class="col-md-3 mt-4">
                                <div class="card" >
                                    <img src="assets/images/products/${product.imgName}" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            <a class="link-edit" href="product-detail?id=${product.id}" style="text-decoration: none; color: black;">${product.name}</a>
                                        </h5>
                                        <c:if test="${product.quantity != 0}">
                                            <a href="cart?id=${product.id}" class="btn btn-primary">Thêm vào giỏ hàng</a>
                                        </c:if>
                                        <c:if test="${product.quantity == 0}">
                                            <a href="" class="btn btn-primary">Sản phẩm đã hết hàng</a>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>        
                    </div>
                </div>
            </div>

        </div>
        <footer>
            <%@include file="component/footer.jsp" %>
        </footer>
        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    </body>
</html>

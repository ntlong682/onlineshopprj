<%-- 
    Document   : edit-product-detail
    Created on : Mar 18, 2021, 3:50:59 PM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/user.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>Edit product's details</title>
    </head>
    <body>
        <%@include file="component/admin-nav.jsp" %>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3">
                    <div class="left-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>Danh mục quản lý</h6>
                            </div>
                        </div>
                        <%@include file="component/admin-sidenav.jsp"%>   
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Chỉnh sửa sản phẩm
                                </h6>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <form action="update-product" acceptcharset="UTF-8" method="post">
                                        <div class="row px-3">
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="id" placeholder="Mã sản phẩm" value="${product.id}" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="name" placeholder="Tên sản phẩm" value="${product.name}">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="category">
                                                    <c:forEach items="${lsCategory}" var="cate">
                                                        <c:if test="${product.categoryId == cate.id}">
                                                            <option value="${cate.id}" selected>${cate.name}</option>
                                                        </c:if>
                                                        <c:if test="${product.categoryId != cate.id}">
                                                            <option value="${cate.id}">${cate.name}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="brand">
                                                    <c:forEach items="${lsBrand}" var="bra">
                                                        <c:if test="${product.brandId == bra.id}">
                                                            <option value="${bra.id}" selected>${bra.brandName}</option>
                                                        </c:if>
                                                        <c:if test="${product.brandId != bra.id}">
                                                            <option value="${bra.id}">${bra.brandName}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="price" placeholder="Giá sản phẩm" value="${product.price}">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="quantity" placeholder="Số lượng sản phẩm" value="${product.quantity}">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="status">
                                                    <c:if test="${product.status == 1}">
                                                        <option value="1" selected>Còn hàng</option>
                                                        <option value="2">Hết hàng</option>
                                                    </c:if>
                                                    <c:if test="${product.status != 1}">
                                                        <option value="1" >Còn hàng</option>
                                                        <option value="2" selected>Hết hàng</option>
                                                    </c:if>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="imgName" placeholder="Tên tệp ảnh" value="${product.imgName}">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <textarea class="form-control" rows="10" name="description" placeholder="Mô tả chi tiêt">${product.descripton}</textarea>
                                            </div>
                                            <input type="submit" class="btn btn-success ml-3" name="btnUpload" value="Cập nhật sản phẩm">
                                        </div>
                                    </form>
                                    <span><p class="notification"> ${message}&nbsp; </p></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>

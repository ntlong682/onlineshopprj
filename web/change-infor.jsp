<%-- 
    Document   : chang-infor
    Created on : Mar 16, 2021, 5:55:46 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/user.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>Reset Password</title>
    </head>
    <body>
        <c:if test="${currentLoginAccount.roleId ne 1}">   
            <%@include file="component/user-navbar.jsp" %>
        </c:if>
        <c:if test="${currentLoginAccount.roleId eq 1}">   
            <%@include file="component/admin-nav.jsp" %>
        </c:if>

        <div class="container">
            <div class="row justify-content-center align-items-baseline mt-5 " style="height:100vh">
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <form action="update-infor" method="post" autocomplete="off">
                                <div class="form-group">
                                    Họ và Tên: <input type="text" class="form-control" name="name" value="${currentAccountDetails.name}">
                                </div>
                                <div class="form-group">
                                    Số điện thoại: <input type="tel" class="form-control" name="mobile" value="${currentAccountDetails.phoneNumber}">
                                </div>
                                <div class="form-group">
                                    Giới tính
                                    <c:if test="${currentAccountDetails.gender eq 1}">
                                        <input type="radio" name="gender" value="1" checked/> Male 
                                        <input type="radio" name="gender" value="0" /> Female <br/>
                                    </c:if>
                                    <c:if test="${currentAccountDetails.gender ne 1}"> 
                                        <input type="radio" name="gender" value="1" /> Male 
                                        <input type="radio" name="gender" value="0" checked/> Female <br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Địa chỉ: <input type="text" class="form-control" name="address" value="${currentAccountDetails.address}">
                                </div>
                                <p class="notification"> ${message}&nbsp; </p>
                                <button type="submit" id="updateInformation" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <%@include file="component/footer.jsp" %>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>

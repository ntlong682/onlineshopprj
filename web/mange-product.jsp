<%-- 
    Document   : mange-product
    Created on : Mar 17, 2021, 3:12:25 AM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/user.css">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>User Detail</title>
    </head>
    <body>
        <%@include file="component/admin-nav.jsp" %>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3">
                    <div class="left-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>Danh mục quản lý</h6>
                            </div>
                        </div>
                        <%@include file="component/admin-sidenav.jsp"%>   
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-side">
                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Thêm sản phẩm
                                </h6>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <form action="addproduct" method="post" acceptcharset="UTF-8">
                                        <div class="row px-3">
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="name" placeholder="Tên sản phẩm">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="category">
                                                    <c:forEach items="${lsCategory}" var="cate">
                                                        <option value="${cate.id}">${cate.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="brand">
                                                    <c:forEach items="${lsBrand}" var="bra">
                                                        <option value="${bra.id}">${bra.brandName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="price" placeholder="Giá sản phẩm">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="quantity" placeholder="Số lượng sản phẩm">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="status">
                                                    <option value="1">Còn hàng</option>
                                                    <option value="2">Hết hàng</option>
                                                </select>
                                            </div>
                                            <!--                                            <div class="form-group col-md-6">
                                                                                            <input type="file" class="form-control-file" name="fileUp" multiple />
                                                                                        </div>-->
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="filename" placeholder="Tên tệp ảnh">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <textarea class="form-control" rows="3" name="description" placeholder="Mô tả chi tiêt"></textarea>
                                            </div>
                                            <input type="submit" class="btn btn-success ml-3" name="btnUpload" value="Thêm sản phẩm">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="row title">
                            <div class="col-md-12">
                                <h6>
                                    Danh sách sản phẩm
                                </h6>
                            </div>
                        </div> 
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="m-info">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">STT</th>
                                                <th scope="col">Mã sản phẩm</th>
                                                <th scope="col">Tên sản phẩm</th>
                                                <th scope="col">Giá tiền</th>
                                                <th scope="col">Số lượng</th>
                                                <th scope="col">Tình trạng</th>
                                                <th scope="col">Thao tác</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${requestScope.lsProduct}" var="product" varStatus="stt">
                                                <tr>
                                                    <td>${stt.index+1}</td>
                                                    <td>${product.id}</td>
                                                    <td>${product.name}</td>
                                                    <td><fmt:formatNumber type="currency" currencySymbol="đ" value="${product.price}" /></td>
                                                    <td>${product.quantity}</td>
                                                    <td>${product.status == 1 ? "Còn hàng" : "Hết hàng"}</td>
                                                    <td>
                                                        <a href="product-detail?id=${product.id}">Chỉnh sửa</a>
                                                    </td>
                                                </tr>
                                            </c:forEach>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>

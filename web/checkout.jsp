<%-- 
    Document   : checkOut
    Created on : Mar 9, 2021, 9:04:15 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <style>

        </style>
        <title>Check Out</title>
    </head>
    <body>
        <%@include file="component/navbar.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="mt-3">Thông tin sản phẩm</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Hình ảnh</th>
                                <th>Tên</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Số tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${sessionScope.listCart}" var="cart">
                                <tr>
                                    <td>
                                        <img src="assets/images/products/${cart.imgName}" style="width: 80px">
                                    </td>
                                    <td>${cart.name}</td>
                                    <td><fmt:formatNumber type="currency" currencySymbol="đ" value="${cart.price}" /></td>

                                    <td>
                                        ${cart.quantity}
                                    </td>
                                    <td>
                                        <fmt:formatNumber type="currency" currencySymbol="đ" value="${cart.price * cart.quantity}" />
                                    </td>
                                    <td>
                                        <a href="remove-cart?id=${cart.id}">Xóa</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="text-end">
                        Tổng tiền:
                        <fmt:formatNumber type="currency" currencySymbol="đ" value="${sessionScope.totalMoney}" />
                    </div>
                    <div>
                        <a class="btn btn-danger mt-4 w-100 btn-lg" href="remove-cart?id=0">Hủy đơn hàng</a>
                    </div>
                </div>
                <div class="col-md-5 ps-5">
                    <h3 class="mt-5 mb-4">Thông tin người mua</h3>
                    <div>
                        <form action="check-out" method="get">
                            <div class="mb-3">
                                <label for="name" class="form-label">Họ và Tên</label>
                                <input type="text" class="form-control" id="name" name="name" value="${currentAccountDetails.name}">
                            </div>
                            <div class="mb-3">
                                <label for="mobile" class="form-label">Số điện thoại</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" value="${currentAccountDetails.phoneNumber}">
                                <!--                                <textarea class="form-control" id="mobile" name="mobile" value=""></textarea>-->
                            </div>
                            <div class="mb-3">
                                <label for="address" class="form-label">Địa chỉ</label>
                                <input type="text" class="form-control" id="address" name="address" value="${currentAccountDetails.address}">
                                <!--                                <textarea class="form-control" id="address" name="address" value=""></textarea>-->
                            </div>
                            <div class="mb-3">
                                <label for="note" class="form-label">Ghi chú</label>
                                <textarea class="form-control" id="note" name="note"></textarea>
                            </div>
                            <button class="btn btn-success mt-2 w-100 btn-lg">Xác nhận</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <%@include file="component/footer.jsp" %>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
</html>

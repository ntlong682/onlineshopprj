<%-- 
    Document   : order-detail
    Created on : Mar 15, 2021, 1:19:57 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <style>
            a {
                text-decoration: none;
            }
        </style>
        <title>Cart</title>
    </head>
    <body>
        <%@include file="component/user-navbar.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Hình ảnh</th>
                                <th>Tên</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Số tiền</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.lsOrderDetail}" var="cart">
                                <tr>
                                    <td>
                                        <img src="assets/images/products/${cart.imgName}" style="width: 80px">
                                    </td>
                                    <td>${cart.name}</td>
                                    <td><fmt:formatNumber type="currency" currencySymbol="đ" value="${cart.price}" /></td>

                                    <td>${cart.quantity}</td>
                                    <td>
                                        <fmt:formatNumber type="currency" currencySymbol="đ" value="${cart.price * cart.quantity}" />
                                    </td>

                                    <td>
                                        <a href="product-detail?id=${cart.id}">Chi tiết sản phẩm</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    </body>
</html>